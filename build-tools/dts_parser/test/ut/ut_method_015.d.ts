/**
 * the ut for method in namespace, method is callback
 */
export namespace test {
  function testFunction(param1: string, callback: AsyncCallback<Want>): void;
}