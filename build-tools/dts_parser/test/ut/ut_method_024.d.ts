/**
 * the ut for method in class, which has params and return value, one of params is optional
 */
export class Test {
  test(param1: string, param2?: string): number;
}