/**
 * the ut for method in class, which has params and return value
 */
export class Test {
  test(param1: string): number;
}