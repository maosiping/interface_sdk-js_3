/**
 * the ut for method in namespace, method is promise
 */
export namespace test {
  function testFunction(param1: string): Promise<Want>;
}