/**
 * the ut for method in namespace, which has params and return value value
 */
export namespace test {
  function testFunction(param1: string): number;
}