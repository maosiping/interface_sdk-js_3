export class Test {
  /**
   * the ut for parent node doesn't have version
   * 
   * @since 6
   */
  id: number;
  /**
   * @since 7
   */
  name: string;
  /**
   * @since 8
   */
  age: number;
}