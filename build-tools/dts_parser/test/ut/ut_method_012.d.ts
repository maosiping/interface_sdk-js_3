/**
 * the ut for method in namespace, which doesn't have params and return value
 */
export namespace test {
  function testFunction(): number
}